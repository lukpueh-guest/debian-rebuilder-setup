CREATE TABLE IF NOT EXISTS BUILDS(
       source TEXT,
       version TEXT,
       timestamp INTEGER,
       metadata TEXT,
       buildinfo TEXT
);
